﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace SplitMoney.QiwiClient
{
    public interface IQiwiSender
    {
        Task<SendingResult> SendAsync(HttpRequestMessage message, CancellationToken cancellation);
    }
}