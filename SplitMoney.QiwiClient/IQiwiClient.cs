﻿using System.Threading;
using System.Threading.Tasks;

namespace SplitMoney.QiwiClient
{
    public interface IQiwiClient
    {
        string Token { get; }
        string Sender { get; }
        Task<QiwiTransaction> SendMoneyAsync(string recipient, decimal amount, CancellationToken cancellation);
    }
}