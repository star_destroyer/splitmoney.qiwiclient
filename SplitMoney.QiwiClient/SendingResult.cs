using System.Net;

namespace SplitMoney.QiwiClient
{
    public class SendingResult
    {
        public SendingResult(bool isSuccess, HttpStatusCode code, string message)
        {
            IsSuccess = isSuccess;
            Code = code;
            Message = message;
        }

        public bool IsSuccess { get; }
        public HttpStatusCode Code { get; }
        public string Message { get; }
    }
}