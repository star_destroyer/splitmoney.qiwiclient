using System.Threading;
using System.Threading.Tasks;

namespace SplitMoney.QiwiClient
{
    public interface IQiwiClientFactory
    {
        Task<string> BeginAuthorizationAsync(string sender, CancellationToken cancellation);

        Task<IQiwiClient> EndAuthorizationAsync(string code, string smsCode, string sender,
            CancellationToken cancellation);

        IQiwiClient Create(string sender, string accessToken);
    }
}