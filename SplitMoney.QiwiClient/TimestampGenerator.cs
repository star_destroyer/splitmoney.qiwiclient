﻿using System;
using System.Globalization;

namespace SplitMoney.QiwiClient
{
    public static class TimestampGenerator
    {
        public static string GetTimestamp() => ((long)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds).ToString(CultureInfo.InvariantCulture);
    }
}