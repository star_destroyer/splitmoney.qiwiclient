using System;
using System.Globalization;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SplitMoney.QiwiClient
{
    public class QiwiClient : IQiwiClient
    {
        private readonly IQiwiSender qiwiSender;
        private static readonly Uri PayUri = new Uri(@"https://sinap.qiwi.com/api/terms/99/payments");

        internal QiwiClient(string sender, string token, IQiwiSender qiwiSender)
        {
            this.qiwiSender = qiwiSender;
            Sender = sender;
            Token = token;
        }

        public string Token { get; }

        public string Sender { get; }

        public async Task<QiwiTransaction> SendMoneyAsync(string recipient, decimal amount,
            CancellationToken cancellation)
        {
            var transactionId = TimestampGenerator.GetTimestamp();
	        var paymentRequestMessage = GetPaymentRequestMessage(transactionId, recipient, amount);
	        var sendResult = await qiwiSender.SendAsync(paymentRequestMessage, cancellation).ConfigureAwait(false);
	        var responseMessage = JsonConvert.DeserializeObject<TransactionMessage>(sendResult.Message);
	        return new QiwiTransaction
            {
                Id = transactionId,
                Sender = Sender,
                Recipient = recipient,
                Amount = amount,
                IsSuccess = sendResult.IsSuccess,
                Message = responseMessage
            };
        }

        private HttpRequestMessage GetPaymentRequestMessage(string id, string recipient, decimal amount)
        {
            var message = new HttpRequestMessage(HttpMethod.Post, PayUri);
            message.Headers.Accept.ParseAdd("application/vnd.qiwi.v2+json");
            message.Headers.AcceptEncoding.ParseAdd("gzip");
            message.Headers.AcceptEncoding.ParseAdd("deflate");
            message.Headers.AcceptEncoding.ParseAdd("compress");
            message.Headers.Authorization =
                new AuthenticationHeaderValue("Token", Base64Encoder.Encode($"{Sender}:{Token}"));
            message.Headers.UserAgent.ParseAdd("HTTPie/0.3.0");

	        var messageContent = GetMessageContent(id, recipient, amount);
	        message.Content = new StringContent(messageContent, Encoding.UTF8, "application/json");
            return message;
        }

        private static string GetMessageContent(string id, string recipient, decimal amount)
        {
            return JsonConvert.SerializeObject(new
            {
                fields = new
                {
                    account = recipient,
                    prvId = "99"
                },
                id = id,
                paymentMethod = new
                {
                    type = "Account",
                    accountId = "643"
                },
                sum = new
                {
                    amount = amount.ToString("F2"),
                    currency = "643"
                }
            });
        }
    }
}