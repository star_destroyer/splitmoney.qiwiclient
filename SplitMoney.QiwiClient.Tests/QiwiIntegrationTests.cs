﻿using System;
using System.Threading;
using Newtonsoft.Json;
using NUnit.Framework;

namespace SplitMoney.QiwiClient.Tests
{
    [TestFixture]
    public class QiwiIntegrationTests
    {
        private const string Sender = "79021510703";
        private static readonly IQiwiSender QiwiSender = new QiwiSender();

        [Test]
        public void BeginAuthorization_ValidPhoneNumber_ReturnCode()
        {
            var qiwiClientFactory = new QiwiClientFactory(QiwiSender);

            var code = qiwiClientFactory.BeginAuthorizationAsync(Sender, CancellationToken.None).Result;

            Console.WriteLine(code);
        }

        [Test]
        public void EndAuthorization_ValidCodeAndSmsCode_ReturnAccessToken()
        {
            var code = "9b25cea0db84fff41a02a44ca4277207";
            var sms = "658604";
            var qiwiClientFactory = new QiwiClientFactory(QiwiSender);

            var qiwiClient = qiwiClientFactory.EndAuthorizationAsync(code, sms, Sender, CancellationToken.None).Result;
            Console.WriteLine(qiwiClient.Token);
        }

        [Test]
        public void Pay_ValidSend_ReturnSuccessTransaction()
        {
            var accessToken = "94637fd8e1fd316fb02222e9ee1e55e3";
            var qiwiClient = new QiwiClientFactory(QiwiSender).Create(Sender, accessToken);

            var transaction = qiwiClient.SendMoneyAsync("79058036949", 10, CancellationToken.None).GetAwaiter().GetResult();
            Console.WriteLine(JsonConvert.SerializeObject(transaction));
            Assert.That(transaction.IsSuccess);
        }
    }
}
